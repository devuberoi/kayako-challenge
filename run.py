import bottle
from app import app
from config import SERVER_CONFIG

bottle.run( app=app,
            host=SERVER_CONFIG['host'],
            port=SERVER_CONFIG['port'],
            debug=SERVER_CONFIG['debug'],
            reloader=SERVER_CONFIG['reloader'] )
