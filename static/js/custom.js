/**
 * function to parse tweets in the main feed on first load
 * @return None
 */
function parseFeed() {
    // Adding methods to type string parse tweet and make it HTML friendly
    String.prototype.parseURL = function() {
        return this.replace(/[A-Za-z]+:\/\/[A-Za-z0-9-_]+\.[A-Za-z0-9-_:%&~\?\/.=]+/g, function(url) {
            return url.link(url);
        });
    };

    String.prototype.parseUsername = function() {
        return this.replace(/[@]+[A-Za-z0-9-_]+/g, function(u) {
            var username = u.replace("@", "");
            return u.link("http://twitter.com/" + username);
        });
    };

    String.prototype.parseHashtag = function() {
        return this.replace(/[#]+[A-Za-z0-9-_]+/g, function(t) {
            var tag = t.replace("#", "%23");
            return t.link("/?q=" + tag);
        });
    };

    var tweets = document.getElementsByClassName('tweet');
    var text = null;
    var parsedText = null;
    for (var i = 0; i < tweets.length; i++) {
        text = tweets[i].innerHTML;
        parsedText = text.parseURL().parseUsername().parseHashtag();
        tweets[i].innerHTML = parsedText;
    }
}

/**
 * function to prepend new tweets to the main feed
 * @param  {object}  data  json response of ajax call
 * @return None
 */
function prependTweets(data) {
    var tweet_box = null;
    var statuses = data.statuses;
    for (var i = statuses.length - 1; i >= 0; i--) {
        tweet_box = '<div class=tweet-box><div class=header><div class=user>@' + statuses[i].user.screen_name + '</div><div class=flex></div><div class=date>' + statuses[i].retweet_count + ' Retweets</div></div><div class=tweet>' + statuses[i].text.parseURL().parseUsername().parseHashtag() + '</div><div class=stats><div class=retweet>' + statuses[i].created_at + '</div><div class=flex></div><div class=view></div></div></div>';
        $('#feed').prepend($(tweet_box).hide());
        $('.tweet-box').slideDown('slow');
    }
}

/**
 * ajax function for long polling tweets
 * @return None
 */
function ajaxFn() {
    $.ajax({
        url: '/refresh',
        type: 'GET',
        dataType: 'json',
        success: function(data) {
            prependTweets(data);
        }
    });
}

// this is where the magic happens (on document load)
$(function() {
    parseFeed();
    var ajaxInterval = 1000 * 60 * 2;
    setInterval(ajaxFn, ajaxInterval);
});
