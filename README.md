# kayako-challenge #

This project aims to complete the twitter challenge given by Kayako

Working Demo : [kayako.uberoi.xyz](http://kayako.uberoi.xyz)

### Problem Statement ###

Write a simple Twitter API client. This client simply has to fetch and display Tweets that 

* Have been re-tweeted at least once and 
* Contains the hashtag *#custserv*

### Stack ###

* python
* bottle web framework
* bottle-beaker
* python-requests

### Details ###

This project leverages Twitter's Application-only authentication model. 
>Twitter offers applications the ability to issue authenticated requests on behalf of the application itself (as opposed to on behalf of a specific user). Twitter’s implementation is based on the Client Credentials Grant flow of the OAuth 2 specification.

### But how do I see it in action? ###
1. Make a new app on dev.twitter.com and get your cosumer key and consumer secret

2. Configure config.py to your preference. See the sample below

```python
  # sample config file
  # Server configuration
  SERVER_CONFIG = {'host': 'localhost',
                   'port': 8080,
                   'debug': False,
                   'reloader': False}

  # Session configuration
  SESSION_CONFIG = {
      'session.type': 'file',
      'session.domain': 'domain.com',
      'session.cookie_expires': 300,
      'session.data_dir': './temp/sessions',
      'session.auto': True
  }

  # Twitter API credentials and Endpoints
  TWITTER_CONSUMER_KEY = 'XXXXXXXXXXXXXXXXX'
  TWITTER_CONSUMER_SECRET = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
  TWITTER_API = {'auth': 'https://api.twitter.com/oauth2/token',
                 'search': 'https://api.twitter.com/1.1/search/tweets.json'}

  # Miscellaneous
  TEMPLATE_PATH = '/home/user/templates'

```

3. Activate python virtual environment and install project requirements
	
	```
	  $ pip install -r requirements.txt
	```

4. Start the project

	```
	  $ python run.py
	```
	
Thats it. No really, that's all that is required to make this project up and running.

### Good to note ###
* Secure your OAuth credentials

* Always serve static files via web server for eg. nginx

	
```
# Sample nginx conf
server {
      listen 80;
      server_name domain.com;
      access_log  /var/log/nginx/access.log;
      proxy_read_timeout 300s;

      location / {
              proxy_pass http://127.0.0.1:8001;
      }

      location /static/ {
             alias /home/www/project/static/;
             autoindex off;
      }
      }
```

* Preferably use virtual environment for a python project

### Why Bottle, why not Flask, Django ? ###
Well, to be honest, Bottle is very much underrated. I prefer Bottle when it comes to quick prototyping, like this project. It is an unopinionated simple framework. I prefer flask when it comes to robust applications but that is a talk for another day. And Django, well, let's say I like myself Unchained. ;)

### How do i buy you a beer?  ###
Tweet [@devuberoi](https://www.twitter.com/devuberoi)