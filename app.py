import bottle
from bottle import route, request, response, hook, template, get, static_file
from beaker.middleware import SessionMiddleware
from twitter import Twitter
from config import SESSION_CONFIG, TEMPLATE_PATH

# initializing a session middleware
app = SessionMiddleware(bottle.app(), SESSION_CONFIG)

# providing a template path
bottle.TEMPLATE_PATH.insert(0, TEMPLATE_PATH)

# pre-request hook to ease session variable access
@hook('before_request')
def before_request():
    request.session = request.environ['beaker.session']

# static files should prefrebly served via a web server such as nginx
# uncomment the next two routes to serve css and js files during development
# @get("/static/css/<filepath:re:.*\.css>")
# def css(filepath):
#     return static_file(filepath, root="static/css")
#
# @get("/static/js/<filepath:re:.*\.js>")
# def css(filepath):
#     return static_file(filepath, root="static/js")

# default route to get access_token for twitter API calls and render the main feed template
@route('/')
def root():
    twitter = Twitter()
    access_token = twitter.authorize()
    params = {'result_type': 'recent'}
    params['q'] = request.query.get('q', '#custserv')
    print params
    if access_token:
        search_response = twitter.search(params=params, filter_tweets=True)
        if search_response:
            request.session['refresh_url'] = search_response['search_metadata']['refresh_url']
            request.session['access_token'] = access_token
            response = template('index', statuses=search_response['statuses'], query=params['q'])
            return response
    response.status = 404;
    return response

# route to serve the refresh calls from main feed via ajax
@route('/refresh')
def tweets():
    access_token = request.session.get('access_token', None)
    refresh_url = request.session.get('refresh_url', None)
    twitter = Twitter(access_token=access_token)

    if access_token is None:
        access_token = twitter.authorize()
        request.session['access_token'] = access_token

    if refresh_url is not None:
        search_response = twitter.search(refresh_url=refresh_url, filter_tweets=True)
        if search_response:
            request.session['refresh_url'] = search_response['search_metadata']['refresh_url']
            data = {"statuses": search_response['statuses']}
            return data
    response.status = 404;
    return response
