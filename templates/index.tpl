<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Kayako - Twitter </title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
    <link rel="stylesheet" href="/static/css/custom.css">
</head>

<body>
    <div class="wrapper">
        <div class="header">
            <img src="https://www.kayako.com/_themes/novo/img/kayako-logos/logo-standard.png" alt="" class="logo">
        </div>
        <div class="info">
          Polling for <span>{{query}}</span>. Feed refreshes every 2 minutes.
        </div>
        <div class="feed" id="feed">
          % for status in statuses:
            <div class="tweet-box">
                <div class="header">
                    <div class="user">
                        @{{status['user']['screen_name']}}
                    </div>
                    <div class="flex"></div>
                    <div class="date">
                        {{status['retweet_count']}} Retweets
                    </div>
                </div>
                <div class="tweet">
                  {{status['text']}}
                </div>
                <div class="stats">
                    <div class="retweet">
                        {{status['created_at']}}
                    </div>
                    <div class="flex"></div>
                    <div class="view"></div>
                </div>
            </div>
          % end
        </div>
    </div>

    <div class="footer">
        <div class="table">
            <span class="cell">Made with love by <a href="https://www.twitter.com/devuberoi" class="handle">@devuberoi</a></span>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js" charset="utf-8" type="text/javascript"></script>
    <script src="/static/js/custom.js" charset="utf-8" type"text/javascript"></script>
</body>

</html>
