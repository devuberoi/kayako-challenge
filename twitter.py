import config
import requests
import json
from urllib import quote as urlencode
from base64 import b64encode
from copy import deepcopy

class Twitter:
    '''Python Class to authorize twitter client and search for tweets'''
    _authorized = False

    # initialization
    def __init__(self, access_token=None):
        setattr(self, 'access_token', access_token)

    # Method to authorize and get access_token for App Only Authorization
    # Twitter allows access to search API via client credentials
    def authorize(self):
        consumer_key = urlencode(config.TWITTER_CONSUMER_KEY)
        consumer_secret = urlencode(config.TWITTER_CONSUMER_SECRET)
        bearer_str = consumer_key + ':' + consumer_secret
        auth_str = 'Basic ' + b64encode(bearer_str)
        headers = {'Authorization': auth_str, 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8.'}
        payload = {'grant_type': 'client_credentials'}
        try:
            response = requests.post(config.TWITTER_API['auth'], headers=headers, data=payload)
            json_response = response.json()
            if response.status_code == 200 and json_response['token_type'] == 'bearer':
                self._authorized = True
                self.access_token = json_response['access_token']
            return self.access_token
        except Exception, e:
            print e
            return None

    # Method to search and refresh via given search parameters
    def search(self, params=None, url=config.TWITTER_API['search'], filter_tweets=False, min_count=1, refresh_url=None):
        if self.access_token is None:
            return False
        if refresh_url is not None:
            url += refresh_url
        try:
            auth_str = 'Bearer ' + self.access_token
            headers = {'Authorization': auth_str}
            response = requests.get(url, params=params, headers=headers)
            json_response = response.json()
            if response.status_code == 200:
                if filter_tweets:
                    json_response = self._filter_retweets(json_response, min_count)
            return json_response
        except Exception, e:
            print e
            return False

    # Internal Method to filter tweets based on number of retweets
    def _filter_retweets(self, json_response, min_count):
        data = deepcopy(json_response)
        filtered_statuses = [status for status in data['statuses'] if status['retweet_count'] >= min_count]
        del data['statuses']
        data['statuses'] = filtered_statuses
        return data
